﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using Microsoft.Graph;
using System.Text;
using MoreLinq;
using System.Configuration;
using IntegritySheet.Models;
using System.Net.Http;
using System.Threading.Tasks;
using Azure.Identity;
using RestSharp;

namespace IntegritySheet
{
    public class Utilities
    {

        private IntegritySheetEntities _context = new IntegritySheetEntities();
     
        public async Task GetUsersAsync()
        {
            try
            {
                var clientId = "6b060636-76a4-46d0-8965-c28574df073e";
                var tenantId = "6662f2ff-6a73-4641-b038-7f16b8ee05c7";
                var SecretKey = "uoG7Q~re9ksCJGb3TtM3uYi~EuItzBsLk5tsp";

                //string[] scopes = { "User.Read" };
                //var options = new InteractiveBrowserCredentialOptions()
                //{
                //    ClientId = clientId,
                //    TenantId = tenantId,
                //    RedirectUri = new Uri("https://localhost:44366")
                //};
                //var interactivecredential = new InteractiveBrowserCredential(options);
                //var graphclient = new GraphServiceClient(interactivecredential, scopes);
                //await interactivecredential.AuthenticateAsync();



                string[] scopes = { ".default" };
                var clientSecretCredential = new ClientSecretCredential(
                    tenantId,
                    clientId,
                    SecretKey);
                var graphClient = new GraphServiceClient(clientSecretCredential, scopes);

                var me = await graphClient.Users["gcushing@bskassociates.com"].Request().GetAsync();
                Console.Write(me.DisplayName);


                //IConfidentialClientApplication confidentialClientApplication = ConfidentialClientApplicationBuilder
                //    .Create(clientId)

                //    .WithTenantId(tenantId)
                //    .WithClientSecret(SecretKey)
                //    .Build();

                //ClientSecretCredential clientSecretCredential = new ClientSecretCredential(tenantId, clientId, SecretKey);
                ////   var authprovider = new ClientCred
                ////   clientSecretCredential
                //GraphServiceClient graphServiceClient = new GraphServiceClient(clientSecretCredential, scopes);

                ////var result = await graphServiceClient.Me.JoinedGroups.Request().GetAsync();



                
             
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException);
                throw;
            }
          
        }
    
        public bool CreateCOCResponses(int id)
        {
           
            //create coc responses based upon a new supplied cooler id
                try
                {
                    foreach (var item in _context.COCQuestions.ToList())
                    {
                    //copy the list of existing questions in the coc questions table into a new response to make a record for the new cooler that has been created
                        var coCResponse = new COCResponse();
                        coCResponse.CoolerID = id;
                        coCResponse.COCQuestionID = item.ID;
                        coCResponse.COCQuestionText = item.Question;
                        _context.COCResponses.Add(coCResponse);
                        _context.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    throw;
                }

                return true;
           
       }
        public bool DeleteRecords(int id)
        {
            try
            {
                var samplebase = _context.SampleLocations.Where(f => f.ID == id).First();
                // get sub elements
                var locationbase = _context.SampleLocationContainers.Where(f => f.SLID == id).ToList();
                foreach (var e in locationbase)
                {             
                    var deletesplits = _context.SplitorPreserves.Where(f => f.SLCID == e.ID).ToList();
                    _context.SplitorPreserves.RemoveRange(deletesplits);
                    _context.SaveChanges();
                }
                _context.SampleLocationContainers.RemoveRange(locationbase);
                _context.SaveChanges();
                _context.SampleLocations.Remove(samplebase);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public bool DeleteRecordsSub(int id)
        {
            try
            {
                var samplebase = _context.SampleLocationContainers.Where(f => f.ID == id).First();
                // get sub elements
                var locationbase = _context.SplitorPreserves.Where(f => f.SLCID == samplebase.ID).ToList();
     
                _context.SplitorPreserves.RemoveRange(locationbase);
                _context.SaveChanges();
                _context.SampleLocationContainers.Remove(samplebase);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }


        public bool CloneSample(int ID,  SampleLocation[] items)
        {
            //update all existing data to follow existing data of existing id selection
            try
            {
                //select the single id. values
                var samplebase = _context.SampleLocations.Where(f => f.ID == ID).First();
                // get sub elements
                var locationbase = _context.SampleLocationContainers.Where(f => f.SLID == ID).ToList();

                //look through all elements
               // var replacebase = _context.SampleLocations.Where(f => f.ID != ID).Where(f=> f.SISID == samplebase.SISID).ToList();
                foreach (var e in items)
                {
                    if (e.ID == ID) {
                        continue;
                    }
                    //copy over any sample location data
                    //delete anything that is there with this slid for a sample

                    var result = _context.SampleLocations.SingleOrDefault(b => b.ID == e.ID);
                    if (result != null)
                    {
                        result.SampledBy = samplebase.SampledBy;
                        result.SampleDate = samplebase.SampleDate;
                        _context.SaveChanges();
                    }
                
                    var replacelocationbase = _context.SampleLocationContainers.Where(f => f.SLID == e.ID).ToList();
                   
                    foreach(var r in replacelocationbase)
                    {
                        var deletesplits = _context.SplitorPreserves.Where(f => f.SLCID == r.ID).ToList();
                        _context.SplitorPreserves.RemoveRange(deletesplits);
                        _context.SaveChanges();
                    }

                    _context.SampleLocationContainers.RemoveRange(replacelocationbase);
                    _context.SaveChanges();

                    //insert all new data
                    foreach (var x in locationbase)
                    {
                        var samplelocation = new SampleLocationContainer();
                        samplelocation.BottleID = x.BottleID;
                        samplelocation.Container = x.Container;
                        samplelocation.Response = x.Response;
                        samplelocation.SampleAmount = x.SampleAmount;
                        samplelocation.SampleType = x.SampleType;
                        samplelocation.SLID = e.ID;
                        samplelocation.SplitorPreserved = x.SplitorPreserved;
                        _context.SampleLocationContainers.Add(samplelocation);
                        _context.SaveChanges();

                        int id = samplelocation.ID;
                        //add subelements for split preserves 
                        var splitbase = _context.SplitorPreserves.Where(f => f.SLCID == x.ID).ToList();
                        foreach (var y in splitbase) 
                        {
                            var splitlocation = new SplitorPreserve();
                            splitlocation.SampleAmount = y.SampleAmount;
                            splitlocation.SplitorPreserveTime = y.SplitorPreserveTime;
                            splitlocation.BottleID = y.BottleID;
                            splitlocation.SLCID = id;
                            splitlocation.SplitorPreserve1 = y.SplitorPreserve1;
                            splitlocation.Preservative = y.Preservative;
                            _context.SplitorPreserves.Add(splitlocation);
                            _context.SaveChanges();
                        }
                    }
                    
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public bool CloneCooler(int ID)
        {
            //update all existing data to follow existing data of existing id selection
            try
            {
                var coolerbase = _context.Coolers.FirstOrDefault(e => e.ID == ID);
               
                int maxbase = _context.Coolers.Count(u => u.SISID == coolerbase.SISID);
                maxbase = maxbase + 1;
                //  coolerbasenew = _context.Coolers.Include("COCResponses").AsNoTracking().FirstOrDefault(e => e.ID == ID);
                //coolerbase.SISID = 339;
                Cooler newcooler = new Cooler();
                newcooler.SISID = coolerbase.SISID;
                newcooler.Description = "Cooler " + maxbase;
                newcooler.Temperature = coolerbase.Temperature;
                newcooler.ShippingMethod = coolerbase.ShippingMethod;
                newcooler.CoolingMethod = coolerbase.CoolingMethod;
                newcooler.ChillingProcessBegun = coolerbase.ChillingProcessBegun;
                newcooler.CustodySeal = coolerbase.CustodySeal;
                newcooler.Courier = coolerbase.Courier;
                newcooler.PackingMaterial = coolerbase.PackingMaterial;
                _context.Coolers.Add(newcooler);
                _context.SaveChanges();

                //add questions from coolerbase
                var cOCResponsebase = _context.COCResponses.Where(e => e.CoolerID == coolerbase.ID).ToList();
                foreach (var x in cOCResponsebase)
                {
                    var cOCResponsenew = new COCResponse();
                    cOCResponsenew.COCQuestionText = x.COCQuestionText;
                    cOCResponsenew.CoolerID = newcooler.ID;
                    cOCResponsenew.ResponseValue = x.ResponseValue;
                    cOCResponsenew.COCQuestionID = x.COCQuestionID;

                    _context.COCResponses.Add(cOCResponsenew);
                    _context.SaveChanges();
                }

                //clone pm notifications

                var PMnotificationbase = _context.PMNotifications.Where(e => e.CoolerID ==coolerbase.ID).ToList();
                foreach (var x in PMnotificationbase)
                {
                    var PMNotiticationNew = new PMNotification();
                    PMNotiticationNew.PMName = x.PMName;
                    PMNotiticationNew.PMTime = x.PMTime;
                    PMNotiticationNew.CoolerID = newcooler.ID;

                    _context.PMNotifications.Add(PMNotiticationNew);
                    _context.SaveChanges();
                }

            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }


        public bool CloneSampleSelected(int ID)
        {
            //update all existing data to follow existing data of existing id selection
            try
            {
                //select the single id. values
                SampleLocation  samplebase = _context.SampleLocations.Where(f => f.ID == ID).First();
                int maxbase = _context.SampleLocations.Max(u => u.ID);
                maxbase = maxbase + 1;
                samplebase.Description = "Location " + maxbase;
               
                _context.SampleLocations.Add(samplebase);
                _context.SaveChanges();
                // get sub elements
                var locationbase = _context.SampleLocationContainers.Where(f => f.SLID == ID).ToList().OrderByDescending(x => x.ID);

                    //insert all new data
                    foreach (var x in locationbase)
                    {
                        var samplelocation = new SampleLocationContainer();
                        samplelocation.BottleID = x.BottleID;
                        samplelocation.Container = x.Container;
                        samplelocation.Response = x.Response;
                        samplelocation.SampleAmount = x.SampleAmount;
                        samplelocation.SampleType = x.SampleType;
                        samplelocation.SLID = maxbase;
                        samplelocation.SplitorPreserved = x.SplitorPreserved;
                        
                        _context.SampleLocationContainers.Add(samplelocation);
                        _context.SaveChanges();
                    }          
            }
            catch (Exception)
            {

                throw;
            }
            return true;
        }

        public static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            // ExpandoObject supports IDictionary so we can extend it like this
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }

        public bool CreateLocations(int id, int amt)
        {

            //create locations based upon the front and multiple sample amount request portion
            try
            {
                for (int i=1; i <= amt; i++ )
                {
                    var sampleLoc = new SampleLocation();
                    sampleLoc.SISID = id;//match passed id to new location
                    //determine naming of description for new sample setups
                    if(amt <= 9 || i <=9 )
                    {
                        sampleLoc.Description = "Location - " + i.ToString("D2");
                    }
                    else
                    {
                        sampleLoc.Description = "Location - " + i;

                    }
                    _context.SampleLocations.Add(sampleLoc);
                    _context.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return true;
        }

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IntegritySheet.Models;
using IntegritySheet.Services;

namespace IntegritySheet.Views
{
    public class SampleLocationsController : Controller
    {
        private IntegritySheetEntities db = new IntegritySheetEntities();

        // GET: SampleLocations
        public ActionResult Index(int ID)
        {
            ViewBag.ID = ID;
            return View();
        }
        [HttpPost]
        public ActionResult CreateSamples(int id, int amt)
        {
            //insert locations to new amounts based upon the passed id and amount
            var Util = new Utilities();
            var created = Util.CreateLocations(id, amt);
            //redirect on jquery on client side .post does not play well with  redirecttoaction
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult CheckValidation(int id)
        {
            //find locations 
            var result = db.SampleLocations.Where(m => m.SISID == id).ToList();
            if (result.Count() == 0)
            {
                return Json(new { error = "Sample Date or Sample By Data missing. Please correct enter sample descriptions", ID = 1 });
            }

            //find each sample location container
            foreach (var response in result)
            {
                // Console.WriteLine(response.ResponseValue);

                var slc = db.SampleLocationContainers.Where(m => m.SLID == response.ID).Count();
                if (slc == 0)
                {
                    return Json(new { error = "Please add containers to unused descriptions", ID = 1 });
                }

                if (response.SampledBy == "" || response.SampleDate is null)
                {
                    return Json(new { error = "Sample Date or Sample By Data missing. Please correct", ID = 1 });
                }
            }
            return Json(new { error = "OK", ID = 2 });
        }

        [HttpPost]
        public ActionResult CloneSample(int id, SampleLocation[] arr)
        {
            //insert locations 
            var Util = new Utilities();
            var created = Util.CloneSample(id, arr);
            //redirect on jquery on client side .post does not play well with  redirecttoaction
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult SendToRobot(int id, string locationhost)
        {

            //post to orchestrator
            var util =  new Utilities();
          
            var response = UIPathAPIService.PostTriggertoOrchestrator(id);
            if (response == "OK")
            {
                return Json(new { error = "OK", ID = 2 });
            }
            else
            {
                return Json(new { error = response, ID = 1 });
            }

            //redirect on jquery on client side .post does not play well with  redirecttoaction
      
        }

        [HttpPost]
        public ActionResult DeleteRecords(int id)
        {
            //delete sample location id 
            var Util = new Utilities();
            var created = Util.DeleteRecords(id);
            //redirect on jquery on client side .post does not play well with  redirecttoaction
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }






        public ActionResult CloneSampleSelected(int id)
        {
            //clone single sample selected
            var Util = new Utilities();
            var created = Util.CloneSampleSelected(id);
            //redirect on jquery on client side .post does not play well with  redirecttoaction
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        // GET: SampleLocations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleLocation sampleLocation = db.SampleLocations.Find(id);
            if (sampleLocation == null)
            {
                return HttpNotFound();
            }
            return View(sampleLocation);
        }

        // GET: SampleLocations/Create
        public ActionResult Create()
        {
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder");
            return View();
        }

        // POST: SampleLocations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SISID,Description")] SampleLocation sampleLocation)
        {
            if (ModelState.IsValid)
            {
                db.SampleLocations.Add(sampleLocation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", sampleLocation.SISID);
            return View(sampleLocation);
        }

        // GET: SampleLocations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleLocation sampleLocation = db.SampleLocations.Find(id);
            if (sampleLocation == null)
            {
                return HttpNotFound();
            }
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", sampleLocation.SISID);
            return View(sampleLocation);
        }



        // POST: SampleLocations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SISID,Description")] SampleLocation sampleLocation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sampleLocation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", sampleLocation.SISID);
            return View(sampleLocation);
        }

        // GET: SampleLocations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleLocation sampleLocation = db.SampleLocations.Find(id);
            if (sampleLocation == null)
            {
                return HttpNotFound();
            }
            return View(sampleLocation);
        }

        // POST: SampleLocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SampleLocation sampleLocation = db.SampleLocations.Find(id);
            db.SampleLocations.Remove(sampleLocation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

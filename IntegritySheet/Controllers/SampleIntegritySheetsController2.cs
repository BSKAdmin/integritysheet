using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using IntegritySheet.Models;

namespace IntegritySheet.Controllers
{
    [Route("api/SampleIntegritySheetsController2/{action}", Name = "SampleIntegritySheetsController2Api")]
    public class SampleIntegritySheetsController2Controller : ApiController
    {
        private IntegritySheetEntities _context = new IntegritySheetEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {
            var sampleintegritysheets = _context.SampleIntegritySheets.Select(i => new {
                i.ID,
                i.WorkOrder,
                i.CreatedBy,
                i.SampleDate,
          //      i.Temperature,
                i.Customer,
                i.Project
            });

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(sampleintegritysheets, loadOptions));
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {
            var model = new SampleIntegritySheet();
            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            var result = _context.SampleIntegritySheets.Add(model);
            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.SampleIntegritySheets.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.SampleIntegritySheets.FirstOrDefaultAsync(item => item.ID == key);

            _context.SampleIntegritySheets.Remove(model);
            await _context.SaveChangesAsync();
        }


        private void PopulateModel(SampleIntegritySheet model, IDictionary values) {
            string ID = nameof(SampleIntegritySheet.ID);
            string WORK_ORDER = nameof(SampleIntegritySheet.WorkOrder);
            string CREATED_BY = nameof(SampleIntegritySheet.CreatedBy);
            string SAMPLE_DATE = nameof(SampleIntegritySheet.SampleDate);
        //    string TEMPERATURE = nameof(SampleIntegritySheet.Temperature);
            string CUSTOMER = nameof(SampleIntegritySheet.Customer);
            string PROJECT = nameof(SampleIntegritySheet.Project);

            if(values.Contains(ID)) {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if(values.Contains(WORK_ORDER)) {
                model.WorkOrder = Convert.ToString(values[WORK_ORDER]);
            }

            if(values.Contains(CREATED_BY)) {
                model.CreatedBy = Convert.ToString(values[CREATED_BY]);
            }

            if(values.Contains(SAMPLE_DATE)) {
                model.SampleDate = values[SAMPLE_DATE] != null ? Convert.ToDateTime(values[SAMPLE_DATE]) : (DateTime?)null;
            }


            if(values.Contains(CUSTOMER)) {
                model.Customer = Convert.ToString(values[CUSTOMER]);
            }

            if(values.Contains(PROJECT)) {
                model.Project = Convert.ToString(values[PROJECT]);
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
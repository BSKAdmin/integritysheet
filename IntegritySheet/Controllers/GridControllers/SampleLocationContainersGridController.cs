using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using IntegritySheet.Models;

namespace IntegritySheet.Controllers
{
    [Route("api/SampleLocationContainersGrid/{action}", Name = "SampleLocationContainersGridApi")]
    public class SampleLocationContainersGridController : ApiController
    {
        private IntegritySheetEntities _context = new IntegritySheetEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {
            var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

            int SLID;
            bool success = Int32.TryParse(queryParams["SLID"], out SLID);
            if (!success)
            {
                SLID = 0;
            }

            var samplelocationcontainers = _context.SampleLocationContainers.Select(i => new {
                i.ID,
                i.SLID,
                i.BottleID,
                i.Response,
                i.SplitorPreserved,
                i.SampleType,
                i.SampleAmount,
                
               //i.Container.SampleLocationContainers,
               i.Container.Bottle ,
               i.Container.Check
                //i.Container.Check
            }).Where(x => x.SLID == SLID);


            using (var context = new IntegritySheetEntities())
            {

                foreach (var samplecontainer in samplelocationcontainers.ToList())
                {
                    var splitbase = _context.SplitorPreserves.Where(f => f.SLCID == samplecontainer.ID).ToList();



                    try
                    {
                        if (splitbase.Count() > 0)
                        {
                            var dep = _context.SampleLocationContainers.Where(i => i.ID == samplecontainer.ID).First();
                            dep.SplitorPreserved = "Y";
                            _context.SaveChanges();
                        }
                        else
                        {
                            var dep = _context.SampleLocationContainers.Where(i => i.ID == samplecontainer.ID).First();
                            dep.SplitorPreserved = "N";
                            _context.SaveChanges();
                        }
                    }
                    catch (Exception)
                    {
                        //Console.WriteLine(Exception)
                        throw;
                    }


                }

            }

            // var count = _context.SampleLocationContainers.Where(x => x.SLID == SLID).SelectMany(0)


             samplelocationcontainers = _context.SampleLocationContainers.Select(i => new {
                i.ID,
                i.SLID,
                i.BottleID,
                i.Response,
                i.SplitorPreserved,
                i.SampleType,
                i.SampleAmount,

                //i.Container.SampleLocationContainers,
                i.Container.Bottle,
                i.Container.Check
                //i.Container.Check
            }).Where(x => x.SLID == SLID);

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(samplelocationcontainers, loadOptions));
        }


    

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {

            //parse inputs into sample location
            var model = new SampleLocationContainer();
            var Slid = form["SLID"]; // sample location id
            var ReturnUID = form["RETURNID"]; //sample location page to return to
            var Bottle = form["MainBottle"];
            var Response = form["Response"];
            var Preserve = form["SplitorPreserved"];
            var SampleType = form["SampleType"];
            var SampleAmount = form["SampleAmount"];
            var SplitAmount = form["SplitAmount"];
            var SplitBottle = form["SplitBottle"];
            //parse checks of input
            int SLID;
            bool success = Int32.TryParse(Slid, out SLID);
            if (!success)
            {
                SLID = 0;
            }

            int ReturnID;
            bool returnsuccess = Int32.TryParse(ReturnUID, out ReturnID);
            if (!returnsuccess)
            {
                ReturnID = 0;
            }

            

            int SplitBottleID;
            bool splitbottlesuccess = Int32.TryParse(SplitBottle, out SplitBottleID);
            if (!splitbottlesuccess)
            {
                SplitBottleID = 0;
            }

            int SampleAmountID;
            bool sampleamountsuccess = Int32.TryParse(SampleAmount, out SampleAmountID);
            if (!sampleamountsuccess)
            {
                SampleAmountID = 0;
            }

            int SplitAmountID;
            bool splitamountsuccess = Int32.TryParse(SplitAmount, out SplitAmountID);
            if (!sampleamountsuccess)
            {
                SplitAmountID = 0;
            }

            if (Preserve == "X" || Preserve== "S")
            {

                for( int i = 1;  i <= SplitAmountID; i = i + 1 )
                {
                    model.SLID = SLID;
                    model.BottleID = SplitBottleID;
                    model.Response = Response;
                    model.SplitorPreserved = Preserve;
                    model.SampleType = SampleType;
                    model.SampleAmount = 1;
                    Validate(model);
                    if (!ModelState.IsValid)
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

                    var splitresult = _context.SampleLocationContainers.Add(model);
                    await _context.SaveChangesAsync();
                }   
                //we are splitting this, pass the split page the id of the sample location
            }
            model.SLID = SLID;
            var bottleid = _context.Containers.Where(e => e.Bottle == Bottle).FirstOrDefault();

            model.BottleID = bottleid.ID;
            model.Response = Response;
            model.SplitorPreserved = Preserve;
            model.SampleType = SampleType;
            model.SampleAmount = 1;
            
            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            //input sample location containers based upon sample id
            using (var context = new IntegritySheetEntities())
            {
                for (int i = 1; i <= SampleAmountID; i++)
                {
                    var result = new SampleLocationContainer();
                    result.SLID = SLID;
                    result.BottleID = bottleid.ID;
                    result.Response = Response;
                    result.SplitorPreserved = Preserve;
                    result.SampleType = SampleType;
                    result.SampleAmount = 1;
                    _context.SampleLocationContainers.Add(result);
                    _context.SaveChanges();
                }
            }
            //return to client that all is well and we need to move to the previous screen
            var response = Request.CreateResponse(HttpStatusCode.Moved);

            string fullyQualifiedUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
            response.Headers.Location = new Uri(fullyQualifiedUrl + "/SampleLocations/Index/" + ReturnID);
            return response;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetSPBottleData(DataSourceLoadOptions loadOptions)
        {
            var bottle = _context.Containers.Select(s => new {  s.Bottle, s.ID, s.Check });

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(bottle, loadOptions));
        }
        
        [HttpGet]
        public HttpResponseMessage GetSPData(DataSourceLoadOptions loadOptions)
        {
            return Request.CreateResponse(DataSourceLoader.Load(SPData.SP, loadOptions));
        }

        [HttpGet]
        public HttpResponseMessage GetSampleData(DataSourceLoadOptions loadOptions)
        {
            return Request.CreateResponse(DataSourceLoader.Load(SampleTypeData.SampleSoils, loadOptions));
        }

        [HttpGet]
        public HttpResponseMessage GetResponseData(DataSourceLoadOptions loadOptions)
        {
            return Request.CreateResponse(DataSourceLoader.Load(ResponseData.Responses, loadOptions));
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.SampleLocationContainers.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form) {

            var key = Convert.ToInt32(form.Get("key"));
            //delete sample location id 
            var Util = new Utilities();
                var created = Util.DeleteRecordsSub(key);
            

         
        }

        [HttpGet]
        public async Task<HttpResponseMessage> ContainersLookup(DataSourceLoadOptions loadOptions) {
            var lookup = from i in _context.Containers
                         orderby i.Bottle
                         select new {
                             Value = i.ID,
                             Text = i.Bottle
                         };
            return Request.CreateResponse(await DataSourceLoader.LoadAsync(lookup, loadOptions));
        }

        [HttpGet]
        public async Task<HttpResponseMessage> SampleLocationsLookup(DataSourceLoadOptions loadOptions) {
            var lookup = from i in _context.SampleLocations
                         orderby i.Description
                         select new {
                             Value = i.ID,
                             Text = i.Description
                         };
            return Request.CreateResponse(await DataSourceLoader.LoadAsync(lookup, loadOptions));
        }

        private void PopulateModel(SampleLocationContainer model, IDictionary values) {
            string ID = nameof(SampleLocationContainer.ID);
            string SLID = nameof(SampleLocationContainer.SLID);
            string BOTTLE_ID = nameof(SampleLocationContainer.BottleID);
            string RESPONSE = nameof(SampleLocationContainer.Response);
            string SPLITOR_PRESERVED = nameof(SampleLocationContainer.SplitorPreserved);
            string SAMPLETYPE = nameof(SampleLocationContainer.SampleType);
            string SAMPLEAMOUNT = nameof(SampleLocationContainer.SampleAmount);

            if (values.Contains(ID)) {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if(values.Contains(SLID)) {
                model.SLID = Convert.ToInt32(values[SLID]);
            }

            if(values.Contains(BOTTLE_ID)) {
                model.BottleID = values[BOTTLE_ID] != null ? Convert.ToInt32(values[BOTTLE_ID]) : (int?)null;
            }

            if (values.Contains(SAMPLEAMOUNT))
            {
                model.SampleAmount = values[SAMPLEAMOUNT] != null ? Convert.ToInt32(values[SAMPLEAMOUNT]) : (int?)null;
            }

            if (values.Contains(RESPONSE)) {
                model.Response = Convert.ToString(values[RESPONSE]);
            }

            if (values.Contains(SAMPLETYPE))
            {
                model.SampleType = Convert.ToString(values[SAMPLETYPE]);
            }

            if (values.Contains(SPLITOR_PRESERVED)) {
                model.SplitorPreserved = Convert.ToString(values[SPLITOR_PRESERVED]);
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
            }
    }
}
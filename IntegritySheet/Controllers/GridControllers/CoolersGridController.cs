using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using IntegritySheet.Models;

namespace IntegritySheet.Controllers
{
    [Route("api/CoolersGrid/{action}", Name = "CoolersGridApi")]
    public class CoolersGridController : ApiController
    {
        private IntegritySheetEntities _context = new IntegritySheetEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {
            var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

            int SISID;

            bool success = Int32.TryParse(queryParams["SISID"], out SISID);
            if (!success)
            {
                SISID = 0;
            }

            var coolers = _context.Coolers.Select(i => new {
                i.ID,
                i.Description,
                i.Temperature,
                i.SISID,
                i.CoolingMethod,
                i.ShippingMethod,
                i.CustodySeal,
                i.ChillingProcessBegun,
                i.Courier,
                i.PackingMaterial
            }).Where(x => x.SISID == SISID);

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(coolers, loadOptions));
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {
            var model = new Cooler();

            //determine if  sisid was passed and then convert it to be added to cooler
            var parameterContext = form["SISID"];

            int SISID;
            bool success = Int32.TryParse(parameterContext, out SISID);
            if (!success)
            {
                SISID = 0;
            }
            model.SISID = SISID;
            //add to model
            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            values.Add("SISID", SISID);
      
            PopulateModel(model, values);
          
            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            var result = _context.Coolers.Add(model);
            await _context.SaveChangesAsync();

            var Util = new Utilities();
            var created = Util.CreateCOCResponses(result.ID);
            return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.Coolers.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            //Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.Coolers.FirstOrDefaultAsync(item => item.ID == key);
            
            var pm =  _context.PMNotifications.Where(x => x.CoolerID == key).ToList();
            _context.PMNotifications.RemoveRange(pm);
            await _context.SaveChangesAsync();

            var coc = _context.COCResponses.Where(x => x.CoolerID == key).ToList();
            _context.COCResponses.RemoveRange(coc);
            await _context.SaveChangesAsync();

            _context.Coolers.Remove(model);
            await _context.SaveChangesAsync();
        }



        private void PopulateModel(Cooler model, IDictionary values) {
            string ID = nameof(Cooler.ID);
            string DESCRIPTION = nameof(Cooler.Description);
            string TEMPERATURE = nameof(Cooler.Temperature);
            string SISID = nameof(Cooler.SISID);
            string SHIPPINGMETHOD = nameof(Cooler.ShippingMethod);
            string COOLINGMETHOD = nameof(Cooler.CoolingMethod);
            string CHILLINGPROCESSBEGUN = nameof(Cooler.ChillingProcessBegun);
            string CUSTODYSEAL = nameof(Cooler.CustodySeal);
            string COURIER = nameof(Cooler.Courier);
            string PACKINGMATERIAL = nameof(Cooler.PackingMaterial);

            if (values.Contains(ID)) {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if(values.Contains(DESCRIPTION)) {
                model.Description = Convert.ToString(values[DESCRIPTION]);
            }
            
            if (values.Contains(COURIER))
            {
                model.Courier = Convert.ToString(values[COURIER]);
            }

            if (values.Contains(PACKINGMATERIAL))
            {
                model.PackingMaterial = Convert.ToString(values[PACKINGMATERIAL]);
            }


            if (values.Contains(TEMPERATURE)) {
                model.Temperature = values[TEMPERATURE] != null ? Convert.ToDecimal(values[TEMPERATURE]) : (decimal?)null;
            }

            if(values.Contains(SISID)) {
                model.SISID = values[SISID] != null ? Convert.ToInt32(values[SISID]) : (int?)null;
            }


            if (values.Contains(CUSTODYSEAL))
            {
                model.CustodySeal = values[CUSTODYSEAL] != null ? Convert.ToInt32(values[CUSTODYSEAL]) : (int?)null;
            }


            if (values.Contains(CHILLINGPROCESSBEGUN))
            {
                model.ChillingProcessBegun = values[CHILLINGPROCESSBEGUN] != null ? Convert.ToInt32(values[CHILLINGPROCESSBEGUN]) : (int?)null;
            }

            if (values.Contains(SHIPPINGMETHOD))
            {
                model.ShippingMethod = Convert.ToString(values[SHIPPINGMETHOD]);
            }

            if (values.Contains(COOLINGMETHOD))
            {
                model.CoolingMethod = Convert.ToString(values[COOLINGMETHOD]);
            }

        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using IntegritySheet.Models;


namespace IntegritySheet.Controllers
{
    [Route("api/SplitorPreservesGrid/{action}", Name = "SplitorPreservesGridApi")]
    public class SplitorPreservesGridController : ApiController
    {
        private IntegritySheetEntities _context = new IntegritySheetEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions)
        {


            var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

            int SLCID;

            bool success = Int32.TryParse(queryParams["SLCID"], out SLCID);
            if (!success)
            {
                SLCID = 0;
            }


            var splitorpreserves = _context.SplitorPreserves.Select(i => new {
                i.ID,
                i.SLCID,
                i.SplitorPreserve1,
                i.Preservative,
                i.SplitorPreserveTime,
                i.BottleID,
                i.SampleAmount
            }).Where(x => x.SLCID == SLCID);

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(splitorpreserves, loadOptions));
        }


        public async Task<HttpResponseMessage> GetSISID(DataSourceLoadOptions loadOptions)
        {


            var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

            int SISID;

            bool success = Int32.TryParse(queryParams["SISID"], out SISID);
            if (!success)
            {
                SISID = 0;
            }




            try
            {
                var splittest = _context.SplitorPreserves.Where(x => x.SampleLocationContainer.SampleLocation.SISID == SISID).Select(i => new
                {
                    i.ID,
                    i.SampleLocationContainer.SampleLocation.Description,
                    i.SampleLocationContainer.Container,
                    i.SLCID,
                    i.SplitorPreserve1,
                    i.Preservative,
                    i.SplitorPreserveTime,
                    i.BottleID,
                    i.SampleAmount
                });
                return Request.CreateResponse(await DataSourceLoader.LoadAsync(splittest, loadOptions));
            }
            catch (Exception e)
            {

                var error = e.InnerException;
             //   return Request.CreateResponse(await DataSourceLoader.LoadAsync(splittest, loadOptions));
                throw;
            }
        
            //get all sample locations for sisid
            //var samplelocations = _context.SampleLocations.Where(m => m.SISID == SISID).ToList();
            //int[] samplecontainerlist = new int[samplelocations.Count()];
            //int q = 0;
            //foreach (var location in samplelocations)
            //{
            //    var samplelocationcontainers = _context.SampleLocationContainers.Where(m => m.SLID == location.ID).FirstOrDefault();
            //    samplecontainerlist[q] = samplelocationcontainers.ID;
            //}

            //var items = _context.SplitorPreserves.Where(item => samplecontainerlist.Contains(item.ID));



            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;

         
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form)
        {

            var model = new SplitorPreserve();
            var parameterContext = form["SLCID"];
            int SLCID;
            bool success = Int32.TryParse(parameterContext, out SLCID);
            if (!success)
            {
                SLCID = 0;
            }
            model.SLCID = SLCID;

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            values.Add("SLCID", SLCID);

            

            PopulateModel(model, values);

            if(model.SampleAmount>1)
            {
                Validate(model);

                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));
                var initsampleamt = model.SampleAmount;
                for (int i = 0; i < initsampleamt; i++)
                {
                    model.SampleAmount = 1;
                    var result = _context.SplitorPreserves.Add(model);
                    await _context.SaveChangesAsync();

                }

               
                return Request.CreateResponse(HttpStatusCode.Created);

            }
            else
            {
                Validate(model);
                if (!ModelState.IsValid)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

                var result = _context.SplitorPreserves.Add(model);
                await _context.SaveChangesAsync();

                return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });

            }

        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form)
        {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.SplitorPreserves.FirstOrDefaultAsync(item => item.ID == key);
            if (model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            //Validate(model);
          //  if (!ModelState.IsValid)
//return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form)
        {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.SplitorPreserves.FirstOrDefaultAsync(item => item.ID == key);

            _context.SplitorPreserves.Remove(model);
            await _context.SaveChangesAsync();
        }


        [HttpGet]
        public async Task<HttpResponseMessage> SampleLocationContainersLookup(DataSourceLoadOptions loadOptions)
        {
            var lookup = from i in _context.SampleLocationContainers
                         orderby i.Response
                         select new
                         {
                             Value = i.ID,
                             Text = i.Response
                         };
            return Request.CreateResponse(await DataSourceLoader.LoadAsync(lookup, loadOptions));
        }

        [HttpGet]
        public async Task<HttpResponseMessage> ContainersLookup(DataSourceLoadOptions loadOptions)
        {
            var lookup = from i in _context.Containers
                         orderby i.Bottle
                         select new
                         {
                             Value = i.ID,
                             Text = i.Bottle
                         };
            return Request.CreateResponse(await DataSourceLoader.LoadAsync(lookup, loadOptions));
        }

        private void PopulateModel(SplitorPreserve model, IDictionary values)
        {
            string ID = nameof(SplitorPreserve.ID);
            string SLCID = nameof(SplitorPreserve.SLCID);
            string SPLITOR_PRESERVE1 = nameof(SplitorPreserve.SplitorPreserve1);
            string PRESERVATIVE = nameof(SplitorPreserve.Preservative);
            string SPLITOR_PRESERVE_TIME = nameof(SplitorPreserve.SplitorPreserveTime);
            string BOTTLE_ID = nameof(SplitorPreserve.BottleID);
            string SAMPLE_AMOUNT = nameof(SplitorPreserve.SampleAmount);

            if (values.Contains(ID))
            {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if (values.Contains(SLCID))
            {
                model.SLCID = values[SLCID] != null ? Convert.ToInt32(values[SLCID]) : (int?)null;
            }

            if (values.Contains(SPLITOR_PRESERVE1))
            {
                model.SplitorPreserve1 = Convert.ToString(values[SPLITOR_PRESERVE1]);
            }

            if (values.Contains(PRESERVATIVE))
            {
                model.Preservative = Convert.ToString(values[PRESERVATIVE]);
            }

            if (values.Contains(SPLITOR_PRESERVE_TIME))
            {
                model.SplitorPreserveTime = values[SPLITOR_PRESERVE_TIME] != null ? Convert.ToDateTime(values[SPLITOR_PRESERVE_TIME]) : (DateTime?)null;
            }

            if (values.Contains(BOTTLE_ID))
            {
                model.BottleID = values[BOTTLE_ID] != null ? Convert.ToInt32(values[BOTTLE_ID]) : (int?)null;
            }

            if (values.Contains(SAMPLE_AMOUNT))
            {
                model.SampleAmount = values[SAMPLE_AMOUNT] != null ? Convert.ToInt32(values[SAMPLE_AMOUNT]) : (int?)null;
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState)
        {
            var messages = new List<string>();

            foreach (var entry in modelState)
            {
                foreach (var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
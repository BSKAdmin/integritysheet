﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using IntegritySheet.Models;
using IntegritySheet.Services;
using Hangfire;

namespace IntegritySheet.Controllers
{
    [Route("api/SampleIntegritySheetsGrid/{action}", Name = "SampleIntegritySheetsGridApi")]
    public class SampleIntegritySheetsGridController : ApiController
    {
        private IntegritySheetEntities _context = new IntegritySheetEntities();
        private ElementEntities _elementcontext = new ElementEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {

            var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

            int STATUSID;

            bool success = Int32.TryParse(queryParams["STATUSID"], out STATUSID);
            if (!success)
            {
                STATUSID = 0;
            }


            if (STATUSID == 99)
            {
                var sampleintegritysheets = _context.SampleIntegritySheets.Select(i => new
                {
                    i.ID,
                     i.CreatedBy,
                    i.ReceiveDate,
                    i.Project,
                    i.StatusID,
                    i.CopyWorkOrder,
                    i.TurnAroundTime,
                    i.Customer,
                    i.WorkOrder,
                    i.ReceivingFacility,
                    i.SubmittedBy
                });
                return Request.CreateResponse(await DataSourceLoader.LoadAsync(sampleintegritysheets, loadOptions));

            }

            if (STATUSID == 3)
            {
                var sampleintegritysheets = _context.SampleIntegritySheets.Select(i => new
                {
                    i.ID,
                    i.CreatedBy,
                    i.ReceiveDate,
                    i.Project,
                    i.StatusID,
                    i.CopyWorkOrder,
                    i.TurnAroundTime,
                    i.Customer,
                    i.WorkOrder,
                    i.ReceivingFacility,
                    i.SubmittedBy
                }).Where(i => i.StatusID == 3 || i.StatusID==8 || i.StatusID ==5);
                return Request.CreateResponse(await DataSourceLoader.LoadAsync(sampleintegritysheets, loadOptions));

            }

            if (STATUSID == 5)
            {
               var  sampleintegritysheets = _context.SampleIntegritySheets.Select(i => new
                {
                    i.ID,
                     i.CreatedBy,
                    i.ReceiveDate,
                    i.Project,
                    i.StatusID,
                    i.CopyWorkOrder,
                    i.TurnAroundTime,
                    i.Customer,
                   i.WorkOrder,
                   i.ReceivingFacility,
                    i.SubmittedBy
               }).Where(i => i.StatusID == 5);
                return Request.CreateResponse(await DataSourceLoader.LoadAsync(sampleintegritysheets, loadOptions));
            }
            else
            {
                var sampleintegritysheets = _context.SampleIntegritySheets.Select(i => new
                {
                    i.ID,
                       i.CreatedBy,
                    i.ReceiveDate,
                    i.Project,
                    i.StatusID,
                    i.CopyWorkOrder,
                    i.TurnAroundTime,
                    i.Customer,
                    i.WorkOrder,
                    i.ReceivingFacility,
                    i.SubmittedBy
                }).Where(i => i.StatusID < 3);
                return Request.CreateResponse(await DataSourceLoader.LoadAsync(sampleintegritysheets, loadOptions));
            }

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;
           
        }



        public HttpResponseMessage GetRobotState(DataSourceLoadOptions loadOptions)
        {

            var robotstate = _context.StatusLookups.Select(i => new
            {
                i.ID,
                i.StatusID,
                i.StatusText

            });

            return Request.CreateResponse(DataSourceLoader.Load(robotstate, loadOptions));
        }

        public HttpResponseMessage GetCreatedByLookUp(DataSourceLoadOptions loadOptions)
        {

            var robotstate = _context.CreatedByLookUps.Select(i => new
            {
                    i.CreatedByName

            });

            return Request.CreateResponse(DataSourceLoader.Load(robotstate, loadOptions));
        }


        public async Task<HttpResponseMessage> GetID(DataSourceLoadOptions loadOptions)
        {
            //get a table by ID

            var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

            int ID;

            bool success = Int32.TryParse(queryParams["ID"], out ID);
            if (!success)
            {
                ID = 0;
            }


 
                var sampleintegritysheets = _context.SampleIntegritySheets.Select(i => new
                {
                    i.ID,
                    i.CreatedBy,
                    i.ReceiveDate,
                    i.Project,
                    i.StatusID,
                    i.CopyWorkOrder,
                    i.TurnAroundTime,
                    i.Customer,
                    i.WorkOrder,
                    i.ReceivingFacility,
                    i.SubmittedBy
                }).Where(i => i.ID == ID);
                return Request.CreateResponse(await DataSourceLoader.LoadAsync(sampleintegritysheets, loadOptions));


        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {
            var model = new SampleIntegritySheet();
            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            model.StatusID = 1;
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));
            try
            {
                var result = _context.SampleIntegritySheets.Add(model);
                await _context.SaveChangesAsync();
                return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });

            }
            catch (Exception e)
            {
                var error = e.InnerException;
                Console.Write(e.InnerException);
                throw;
            }
          
           
        }


        [HttpPost]
        public async Task<HttpResponseMessage> PostDirect(FormDataCollection form)
        {
            var model = new SampleIntegritySheet();
           // var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));

            model.SubmittedBy = form["SubmittedBy"];

            model.ReceiveDate = Convert.ToDateTime(form["ReceiveDate"]);

            model.CopyWorkOrder = form["CopyWorkOrder"];
            model.Customer = form["Customer"];
            model.Project = form["Project"];
            model.CreatedBy = form["CreatedBy"];

            model.TurnAroundTime = Convert.ToInt32(form["TurnAroundTime"]);
            model.ReceivingFacility = Convert.ToInt32(form["ReceivingFacility"]);
            model.StatusID = 1;
         //   PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));
            try
            {
                var result = _context.SampleIntegritySheets.Add(model);
                await _context.SaveChangesAsync();

                var response = Request.CreateResponse(HttpStatusCode.Moved);

                string fullyQualifiedUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                response.Headers.Location = new Uri(fullyQualifiedUrl + "/COCResponses/Index/" + result.ID);
                return response;

            }
            catch (Exception e)
            {
                var error = e.InnerException;
                Console.Write(e.InnerException);
                throw;
            }


        }


        [HttpPost]
        public async Task<HttpResponseMessage> CopySampleWorkOrder(FormDataCollection form)
        {
            var model = new SampleIntegritySheet();
            //var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            model.StatusID = 1;
            //PopulateModel(model, values);
            model.SubmittedBy = form["SubmittedBy"];

            model.ReceiveDate = Convert.ToDateTime(form["ReceiveDate"]);

            model.CopyWorkOrder = form["CopyWorkOrder"];
            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));
            try
            {
                var result = _context.SampleIntegritySheets.Add(model);
                await _context.SaveChangesAsync();
                UIPathAPIService.PostTriggertoOrchestrator(result.ID);
                var response = Request.CreateResponse(HttpStatusCode.Moved);

                string fullyQualifiedUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                response.Headers.Location = new Uri(fullyQualifiedUrl + "/Completed/Final/" + result.ID);
                return response;
            }
            catch (Exception e)
            {
                var error = e.InnerException;
                Console.Write(e.InnerException);
                throw;
            }



        }

        [HttpPost]
        public async Task<HttpResponseMessage> PreLog(FormDataCollection form)
        {
           
            var model = new SampleIntegritySheet();
            // var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));

            model.SubmittedBy = form["SubmittedBy"];

            model.ReceiveDate = Convert.ToDateTime(form["ReceiveDate"]);

            model.WorkOrder = form["WorkOrder"];
            model.Customer = form["Customer"];
            model.Project = form["Project"];
            model.CreatedBy = form["CreatedBy"];

            model.TurnAroundTime = Convert.ToInt32(form["TurnAroundTime"]);
            model.ReceivingFacility = Convert.ToInt32(form["ReceivingFacility"]);
            model.StatusID = 1;




            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));
            try
            {
                var result = _context.SampleIntegritySheets.Add(model);
                await _context.SaveChangesAsync();

                var response = Request.CreateResponse(HttpStatusCode.Moved);

                string fullyQualifiedUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                response.Headers.Location = new Uri(fullyQualifiedUrl + "/COCResponses/Index/" + result.ID);
                return response; 
            }
            catch (Exception e)
            {
                var error = e.InnerException;
                Console.Write(e.InnerException);
                throw;
            }




        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.SampleIntegritySheets.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

           // Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        public HttpResponseMessage GetStatusData(DataSourceLoadOptions loadOptions)
        {
            return Request.CreateResponse(DataSourceLoader.Load(StatusData.Question, loadOptions));
        }

        public HttpResponseMessage GetQuestionData(DataSourceLoadOptions loadOptions)
        {
            return Request.CreateResponse(DataSourceLoader.Load(CopyWorkOrder.Question, loadOptions));
        }

        public HttpResponseMessage GetFacilityData(DataSourceLoadOptions loadOptions)
        {

            var facilitylist = _context.ReceivingFacilities.Select(i => new
            {
                i.ID,
                i.FacilityName
              
            });
          
            return Request.CreateResponse(DataSourceLoader.Load(facilitylist, loadOptions));
        }


        [HttpDelete]
        public async Task Delete(FormDataCollection form) {
            try
            {
                var key = Convert.ToInt32(form.Get("key"));
                var model = await _context.SampleIntegritySheets.FirstOrDefaultAsync(item => item.ID == key);
                //delete cooler data

                var coolers = _context.Coolers.Where(m => m.SISID == key).ToList();
                foreach (var singlecooler in coolers)
                {
                    //delete each cooler response and pm notification for each cooler
                    var cocresponses = _context.COCResponses.Where(m => m.CoolerID == singlecooler.ID).ToList();
                    foreach(var singlecoc in cocresponses)
                    {
                        var pmnotifications = _context.PMNotifications.Where(m => m.CoolerID == singlecooler.ID).ToList();
                        _context.PMNotifications.RemoveRange(pmnotifications);
                        await _context.SaveChangesAsync();

                    }

                    _context.COCResponses.RemoveRange(cocresponses);
                    await _context.SaveChangesAsync();

                }
                _context.Coolers.RemoveRange(coolers);
                await _context.SaveChangesAsync();

                //delete location data
                var samplelocations = _context.SampleLocations.Where(m => m.SISID == key).ToList();
                foreach (var singlelocation in samplelocations)
                {
                    var samplelocationcontainers = _context.SampleLocationContainers.Where(m => m.SLID == singlelocation.ID).ToList();
                    //find splits and delete any thing else as well
                    foreach(var samplelocationcontainer in samplelocationcontainers)
                    {
                        var splitinformation = _context.SplitorPreserves.Where(m => m.SLCID == samplelocationcontainer.ID).ToList();
                        _context.SplitorPreserves.RemoveRange(splitinformation);
                        await _context.SaveChangesAsync();
                    }
                    _context.SampleLocationContainers.RemoveRange(samplelocationcontainers);
                    await _context.SaveChangesAsync();
                }

                //remove comments
                _context.SampleLocations.RemoveRange(samplelocations);
                var comments = _context.Comments.Where(m => m.SISID == key).ToList();
                _context.Comments.RemoveRange(comments);
                await _context.SaveChangesAsync();
               //removal of main 
                _context.SampleIntegritySheets.Remove(model);
                await _context.SaveChangesAsync();

            }
            catch (Exception)
            {

                throw;
            }
        }


        private void PopulateModel(SampleIntegritySheet model, IDictionary values)
        {
            string ID = nameof(SampleIntegritySheet.ID);
            string CREATED_BY = nameof(SampleIntegritySheet.CreatedBy);
            string RECEIVE_DATE = nameof(SampleIntegritySheet.ReceiveDate);
            string CUSTOMER = nameof(SampleIntegritySheet.Customer);
            string PROJECT = nameof(SampleIntegritySheet.Project);
            string COPYWORKORDER = nameof(SampleIntegritySheet.CopyWorkOrder);
            string TURNAROUNDTIME = nameof(SampleIntegritySheet.TurnAroundTime);
            string FACILITYNAME = nameof(SampleIntegritySheet.ReceivingFacility);
            string SUBMITTEDBY = nameof(SampleIntegritySheet.SubmittedBy);
            string STATUSID = nameof(SampleIntegritySheet.StatusID);
            string WORKORDER = nameof(SampleIntegritySheet.WorkOrder);

            if (values.Contains(ID))
            {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if (values.Contains(STATUSID))
            {
                model.StatusID = Convert.ToInt32(values[STATUSID]);
            }

            if (values.Contains(WORKORDER))
            {
                model.WorkOrder = Convert.ToString(values[WORKORDER]);
            }


            if (values.Contains(CREATED_BY))
            {
                model.CreatedBy = Convert.ToString(values[CREATED_BY]);
            }

            if (values.Contains(RECEIVE_DATE))
            {
                model.ReceiveDate = values[RECEIVE_DATE] != null ? Convert.ToDateTime(values[RECEIVE_DATE]) : (DateTime?)null;
            }

            if (values.Contains(TURNAROUNDTIME))
            {
                model.TurnAroundTime = Convert.ToInt32(values[TURNAROUNDTIME]);
            }

            if (values.Contains(FACILITYNAME))
            {
                model.ReceivingFacility = values[FACILITYNAME] != null ? Convert.ToInt32(values[FACILITYNAME]) : (int?)null;
            }

            if (values.Contains(COPYWORKORDER))
            {
                model.CopyWorkOrder = Convert.ToString(values[COPYWORKORDER]);
            }

            if (values.Contains(SUBMITTEDBY))
            {
                model.SubmittedBy = Convert.ToString(values[SUBMITTEDBY]);
            }

            if (values.Contains(CUSTOMER))
            {
                model.Customer = Convert.ToString(values[CUSTOMER]);
            }

            if (values.Contains(PROJECT))
            {
                model.Project = Convert.ToString(values[PROJECT]);
            }
        }


        [HttpGet]
        public async Task<HttpResponseMessage> ElementClientLookup(DataSourceLoadOptions loadOptions)
        {
            var lookup = from i in _elementcontext.CLIENTs
                         
                         select new
                         {
                             Text = i.Client1
                         };

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(lookup, loadOptions));
        }


        [HttpGet]
        public  HttpResponseMessage ElementProjectLookup(DataSourceLoadOptions loadOptions)
        {
            //var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);
            //var client = queryParams["Client"];
          
            var lookup = from i in _elementcontext.PROJECTs.Where(x=>x.isActive==true)

                         select new
                         {
                             Client= i.Client,
                             Text = i.Project1,
                            
                         };
            var test = lookup.ToList().OrderBy(x => x.Text);

            return Request.CreateResponse(DataSourceLoader.Load(test, loadOptions));
        }


        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}



using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using IntegritySheet.Models;
using IntegritySheet.Services;

namespace IntegritySheet.Controllers
{
    [Route("api/SampleLocationsGrid/{action}", Name = "SampleLocationsGridApi")]
    public class SampleLocationsGridController : ApiController
    {
        private IntegritySheetEntities _context = new IntegritySheetEntities();
        
        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {
            //determine if SISID is set in the params of get
            var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);
            int SISID;
            bool success = Int32.TryParse(queryParams["SISID"], out SISID);
            if (!success)
            {
                SISID = 0;
            }

            var samplelocations = _context.SampleLocations.Select(i => new {
                i.ID,
                i.SISID,
                i.Description,
                i.SampledBy,
                i.SampleDate
            }).Where(x => x.SISID == SISID);
            
            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(samplelocations, loadOptions));
        }



        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {
            //determine if SISID is set in the params of get
            var model = new SampleLocation();
            var parameterContext = form["SISID"]; //look for specific form key that isnt a default key
            int SISID;
            bool success = Int32.TryParse(parameterContext, out SISID);
            if (!success)
            {
                SISID = 0;
            }
            model.SISID = SISID;
            
            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            values.Add("SISID", SISID);
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            var result = _context.SampleLocations.Add(model);
            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));//get the form keys
            var parameterContext = form["SISID"]; //look for specific form key that isnt a default key
            var model = await _context.SampleLocations.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            //Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.SampleLocations.FirstOrDefaultAsync(item => item.ID == key);

            _context.SampleLocations.Remove(model);
            await _context.SaveChangesAsync();
        }


        private void PopulateModel(SampleLocation model, IDictionary values) {
            string ID = nameof(SampleLocation.ID);
            string SISID = nameof(SampleLocation.SISID);
            string DESCRIPTION = nameof(SampleLocation.Description);
            string SAMPLEDBY = nameof(SampleLocation.SampledBy);
            string SAMPLEDDATE = nameof(SampleLocation.SampleDate);

            if (values.Contains(ID)) {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if(values.Contains(SISID)) {
                model.SISID = values[SISID] != null ? Convert.ToInt32(values[SISID]) : (int?)null;
            }

            if(values.Contains(DESCRIPTION)) {
                model.Description = Convert.ToString(values[DESCRIPTION]);
            }

            if (values.Contains(SAMPLEDBY))
            {
                model.SampledBy = Convert.ToString(values[SAMPLEDBY]);
            }

            if (values.Contains(SAMPLEDDATE))
            {
                model.SampleDate = values[SAMPLEDDATE] != null ? Convert.ToDateTime(values[SAMPLEDDATE]) : (DateTime?)null;
            }

        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
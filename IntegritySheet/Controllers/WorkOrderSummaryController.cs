﻿using IntegritySheet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rotativa;
using System.IO;

namespace IntegritySheet.Controllers
{
    public class WorkOrderSummaryController : Controller
    {
        private IntegritySheetEntities db = new IntegritySheetEntities();

        // GET: WorkOrderSummary/Details/25
        public ActionResult Details(int? id)
        {
            //if (id == null)
            //{
            //    id = 25;
            //}


            WorkOrderSummary workOrderSummary = new WorkOrderSummary();

            workOrderSummary.WorkOrderSummaryID = id;

            workOrderSummary.spProject = new sp_GetProject_Result();
            workOrderSummary.spProject = db.sp_GetProject(id).FirstOrDefault();

           // workOrderSummary.spProject.WorkOrder = workOrderSummary.spProject.WorkOrder;

            workOrderSummary.spReceipt = new sp_GetReceipt_Result();
            workOrderSummary.spReceipt = db.sp_GetReceipt(id).FirstOrDefault();

            workOrderSummary.containers = db.SampleLocationContainers.Where(m => m.SampleLocation.SISID == id).ToList();
            
            
            workOrderSummary.spCOCResponses = db.sp_GetCOCResponses(id).ToList<sp_GetCOCResponses_Result>();
            workOrderSummary.splitData = db.sp_GetSplitData(id).ToList<sp_GetSplitData_Result>();


            //workOrderSummary.spProject = db.sp_GetProject(id);
            return View(workOrderSummary);
        }

        public ActionResult PrintViewToPdf(int ssid)
        {
            var report = new ActionAsPdf("Details", new { id = ssid } );
         
            var actionPDF = new Rotativa.ActionAsPdf("Details", new { id = ssid })
            {
                FileName = ssid + ".pdf",
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                PageMargins = { Left = 1, Right = 1 }
            };
            byte[] applicationPDFData = actionPDF.BuildFile(ControllerContext);

            //var fileStream = new FileStream("~/Desktop/"+ssid +".pdf", FileMode.Create, FileAccess.Write);
            //fileStream.Write(applicationPDFData, 0, applicationPDFData.Length);
            //fileStream.Close();
     
            return File(applicationPDFData, System.Net.Mime.MediaTypeNames.Application.Octet, "~/Desktop/"+ssid+".pdf");

        }
    }
}

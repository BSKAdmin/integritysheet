﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IntegritySheet.Models;

namespace IntegritySheet.Views
{
    public class PMNotificationsController : Controller
    {
        private IntegritySheetEntities db = new IntegritySheetEntities();

        // GET: PMNotifications
        public ActionResult Index(int ID)
        {
            ViewBag.ID = ID;
            return View();
        }

        // GET: PMNotifications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PMNotification pMNotification = db.PMNotifications.Find(id);
            if (pMNotification == null)
            {
                return HttpNotFound();
            }
            return View(pMNotification);
        }

        // GET: PMNotifications/Create
        public ActionResult Create()
        {
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder");
            return View();
        }

        // POST: PMNotifications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SISID,PMName,PMTime")] PMNotification pMNotification)
        {
            if (ModelState.IsValid)
            {
                db.PMNotifications.Add(pMNotification);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", pMNotification.SISID);
            return View(pMNotification);
        }

        // GET: PMNotifications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PMNotification pMNotification = db.PMNotifications.Find(id);
            if (pMNotification == null)
            {
                return HttpNotFound();
            }
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", pMNotification.SISID);
            return View(pMNotification);
        }

        // POST: PMNotifications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SISID,PMName,PMTime")] PMNotification pMNotification)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pMNotification).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", pMNotification.SISID);
            return View(pMNotification);
        }

        // GET: PMNotifications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PMNotification pMNotification = db.PMNotifications.Find(id);
            if (pMNotification == null)
            {
                return HttpNotFound();
            }
            return View(pMNotification);
        }

        // POST: PMNotifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PMNotification pMNotification = db.PMNotifications.Find(id);
            db.PMNotifications.Remove(pMNotification);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

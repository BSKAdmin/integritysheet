﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IntegritySheet.Models;

namespace IntegritySheet.Controllers
{
    public class SampleIntegritySheetsController : Controller
    {
        private IntegritySheetEntities db = new IntegritySheetEntities();

        // GET: SampleIntegritySheets
        public ActionResult Index()
        {
            var sampleIntegritySheets = db.SampleIntegritySheets.Include(s => s.ReceivingFacility1).Include(s => s.StatusLookup);
            return View(sampleIntegritySheets.ToList());
        }

        // GET: SampleIntegritySheets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleIntegritySheet sampleIntegritySheet = db.SampleIntegritySheets.Find(id);
            if (sampleIntegritySheet == null)
            {
                return HttpNotFound();
            }
            return View(sampleIntegritySheet);
        }

        // GET: SampleIntegritySheets/Create
        public ActionResult Create()
        {
            ViewBag.ReceivingFacility = new SelectList(db.ReceivingFacilities, "ID", "FacilityName");
            ViewBag.StatusID = new SelectList(db.StatusLookups, "StatusID", "StatusText");
            return View();
        }

        // POST: SampleIntegritySheets/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CreatedBy,ReceiveDate,Customer,Project,CopyWorkOrder,StatusID,TurnAroundTime,ReceivingFacility,SubmittedBy,WorkOrder")] SampleIntegritySheet sampleIntegritySheet)
        {
            if (ModelState.IsValid)
            {
                db.SampleIntegritySheets.Add(sampleIntegritySheet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ReceivingFacility = new SelectList(db.ReceivingFacilities, "ID", "FacilityName", sampleIntegritySheet.ReceivingFacility);
            ViewBag.StatusID = new SelectList(db.StatusLookups, "StatusID", "StatusText", sampleIntegritySheet.StatusID);
            return View(sampleIntegritySheet);
        }

        // GET: SampleIntegritySheets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleIntegritySheet sampleIntegritySheet = db.SampleIntegritySheets.Find(id);
            if (sampleIntegritySheet == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReceivingFacility = new SelectList(db.ReceivingFacilities, "ID", "FacilityName", sampleIntegritySheet.ReceivingFacility);
            ViewBag.StatusID = new SelectList(db.StatusLookups, "StatusID", "StatusText", sampleIntegritySheet.StatusID);
            return View(sampleIntegritySheet);
        }

        // POST: SampleIntegritySheets/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CreatedBy,ReceiveDate,Customer,Project,CopyWorkOrder,StatusID,TurnAroundTime,ReceivingFacility,SubmittedBy,WorkOrder")] SampleIntegritySheet sampleIntegritySheet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sampleIntegritySheet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ReceivingFacility = new SelectList(db.ReceivingFacilities, "ID", "FacilityName", sampleIntegritySheet.ReceivingFacility);
            ViewBag.StatusID = new SelectList(db.StatusLookups, "StatusID", "StatusText", sampleIntegritySheet.StatusID);
            return View(sampleIntegritySheet);
        }

        // GET: SampleIntegritySheets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleIntegritySheet sampleIntegritySheet = db.SampleIntegritySheets.Find(id);
            if (sampleIntegritySheet == null)
            {
                return HttpNotFound();
            }
            return View(sampleIntegritySheet);
        }

        // POST: SampleIntegritySheets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SampleIntegritySheet sampleIntegritySheet = db.SampleIntegritySheets.Find(id);
            db.SampleIntegritySheets.Remove(sampleIntegritySheet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IntegritySheet.Models;

namespace IntegritySheet.Views
{
    public class CommentsController : Controller
    {
        private IntegritySheetEntities db = new IntegritySheetEntities();

        // GET: Comments
        public ActionResult Index(int ID)
        {
            ViewBag.ID = ID;
            return View();
        }

        // GET: Comments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // GET: Comments/Create
        public ActionResult Create()
        {
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder");
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SISID,Comment1")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", comment.SISID);
            return View(comment);
        }

        //redirect call and validation  believe not needed
        [HttpPost]
        public ActionResult POST()
        {
            var SISID = Request.Form["SISID"];
            try
            {

                var PMName = Request.Form["PMName"];
                var PMDate = Request.Form["PMDate"];

                if (!String.IsNullOrEmpty(PMName))
                {
                    var res = new PMNotification();
                    res.SISID = Int32.Parse(SISID);
                    res.PMName = PMName;
                    if (!String.IsNullOrEmpty(PMDate))
                    {
                        var parsedDate = DateTime.Parse(PMDate);
                        res.PMTime = parsedDate;
                    }

                    db.PMNotifications.Add(res);
                    db.SaveChanges();
                }

                return RedirectToAction("Index/" + SISID, "SampleLocations");
            }

            catch (Exception)
            {
                ModelState.AddModelError("name", "Please enter in a correct time and PMName");
                return RedirectToAction("Index/" + SISID, "COCResponses");
            }
        }

        // GET: Comments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", comment.SISID);
            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SISID,Comment1")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", comment.SISID);
            return View(comment);
        }

        // GET: Comments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

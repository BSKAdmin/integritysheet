﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IntegritySheet.Models;

namespace IntegritySheet.Views
{
    public class SplitorPreservesController : Controller
    {
        private IntegritySheetEntities db = new IntegritySheetEntities();

        // GET: SplitorPreserves
        public ActionResult Index(int ID)
        {
            ViewBag.ID = ID;
           SampleLocationContainer sp  = db.SampleLocationContainers.First(m => m.ID == ID);
            ViewBag.Name = sp.Container.Bottle;
          //  ViewBag.Location = sp.SampleAmount;
            return View();
        }

        // GET: SplitorPreserves/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SplitorPreserve splitorPreserve = db.SplitorPreserves.Find(id);
            if (splitorPreserve == null)
            {
                return HttpNotFound();
            }
            return View(splitorPreserve);
        }

        // GET: SplitorPreserves/Create
        public ActionResult Create()
        {
            ViewBag.SLCID = new SelectList(db.SampleLocationContainers, "ID", "Response");
            return View();
        }

        // POST: SplitorPreserves/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SLCID,BottleID,SplitorPreserve1,Preservative,SplitorPreserveTime")] SplitorPreserve splitorPreserve)
        {
            if (ModelState.IsValid)
            {
                db.SplitorPreserves.Add(splitorPreserve);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SLCID = new SelectList(db.SampleLocationContainers, "ID", "Response", splitorPreserve.SLCID);
            return View(splitorPreserve);
        }

        // GET: SplitorPreserves/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SplitorPreserve splitorPreserve = db.SplitorPreserves.Find(id);
            if (splitorPreserve == null)
            {
                return HttpNotFound();
            }
            ViewBag.SLCID = new SelectList(db.SampleLocationContainers, "ID", "Response", splitorPreserve.SLCID);
            return View(splitorPreserve);
        }

        // POST: SplitorPreserves/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SLCID,BottleID,SplitorPreserve1,Preservative,SplitorPreserveTime")] SplitorPreserve splitorPreserve)
        {
            if (ModelState.IsValid)
            {
                db.Entry(splitorPreserve).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SLCID = new SelectList(db.SampleLocationContainers, "ID", "Response", splitorPreserve.SLCID);
            return View(splitorPreserve);
        }

        // GET: SplitorPreserves/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SplitorPreserve splitorPreserve = db.SplitorPreserves.Find(id);
            if (splitorPreserve == null)
            {
                return HttpNotFound();
            }
            return View(splitorPreserve);
        }

        // POST: SplitorPreserves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SplitorPreserve splitorPreserve = db.SplitorPreserves.Find(id);
            db.SplitorPreserves.Remove(splitorPreserve);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

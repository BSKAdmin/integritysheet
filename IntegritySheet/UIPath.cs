﻿using IntegritySheet.Models;
using System;
using System.Linq;
using RestSharp;
using Newtonsoft.Json;
using System.Data.Entity;
using System.Configuration;
using System.Collections.Specialized;

namespace IntegritySheet.Services
{
    public class UIPathAPIService
    {

        public static string PostTriggertoOrchestrator(int sequenceTriggerID)
        {
            IntegritySheetEntities db = new IntegritySheetEntities();
            db.Configuration.ProxyCreationEnabled = false;
            
            //Get Token From Orchestrator
            try
            {
                var client = new RestClient("https://account.uipath.com/oauth/token?");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("X-UIPATH-TenantName", "BSKAssociatesDefault");
                request.AddHeader("Content-Type", "application/json");

                var tokenRequest = new TokenRequest();
                tokenRequest.grant_type = "refresh_token";
                tokenRequest.client_id = "8DEv1AMNXczW3y4U15LL3jYf62jK93n5";
                tokenRequest.refresh_token = "_JtGrbVE-T1uYMbXbL3lOD7A2iiV55UAO24osS9Cj8BVk";

                var jsonBody = JsonConvert.SerializeObject(tokenRequest);
                var body = jsonBody;



                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);

                TokenResponse tokenResponse;


                if (response.IsSuccessful)
                {
                    tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(response.Content);
                }
                else
                {
                    string errorBody = response.Content;
                    Exception exception = new Exception(errorBody);
                    return exception.ToString();
                }

                //Post Object to Orchestror
                try
                {

                    var postClient = new RestClient("https://cloud.uipath.com/bskassociates/BSKAssociatesDefault");
                    var postRequest = new RestRequest("/odata/Queues/UiPathODataSvc.AddQueueItem", Method.POST);
                    postRequest.AddHeader("Authorization", string.Format("Bearer {0}", tokenResponse.access_token));
                    postRequest.AddHeader("X-UIPATH-TenantName", "BSKAssociatesDefault");
                    postRequest.AddHeader("X-UIPATH-OrganizationUnitId", ConfigurationManager.AppSettings["OrganizationUnitId"]);
                    postRequest.AddHeader("Content-Type", "application/json");

                    postRequest.RequestFormat = DataFormat.Json;

                    dynamic addQueueItem = new System.Dynamic.ExpandoObject();
                    addQueueItem.itemData = new System.Dynamic.ExpandoObject();
                    var queuename = ConfigurationManager.AppSettings["UIPathQueueName"];

                    Console.WriteLine(queuename);
                    Console.WriteLine(ConfigurationManager.AppSettings.Get("OrganizationUnitId"));
                    addQueueItem.itemData.Name = ConfigurationManager.AppSettings["UIPathQueueName"];



                    addQueueItem.itemData.Reference = sequenceTriggerID.ToString();
                    addQueueItem.itemData.SpecificContent = new System.Dynamic.ExpandoObject();

                    addQueueItem.itemData.SpecificContent.ID = 1;
                    //set workflow status for                                    z
                    var SIS = db.SampleIntegritySheets.Where(e => e.ID == sequenceTriggerID).FirstOrDefault();
                    string Workflow = "";
                    if ((SIS.WorkOrder is null) && (SIS.CopyWorkOrder is null))
                    {
                        Workflow = "Naked";
                    }
                    else if (SIS.WorkOrder != null)
                    {
                        Workflow = "PreLog";
                    }
                    else if (SIS.CopyWorkOrder != null)
                    {
                        Workflow = "Copy";
                    }
                    else
                    {
                        Workflow = "Error";
                    }

                    Utilities.AddProperty(addQueueItem.itemData.SpecificContent, "SSID", sequenceTriggerID);
                    Utilities.AddProperty(addQueueItem.itemData.SpecificContent, "WorkOrderType", Workflow);
                    //build post body of elements
                    var postjsonBody = JsonConvert.SerializeObject(addQueueItem);
                    var postbody = postjsonBody;

                    //set element headers
                    postRequest.AddParameter("application/json", postbody, ParameterType.RequestBody);
                    IRestResponse Postresponse = postClient.Execute(postRequest);
                    if (Postresponse.IsSuccessful)
                    {

                        //change status of bot if post is successful
                        using (var _context = new IntegritySheetEntities())
                        {
                            var result = db.SampleIntegritySheets.SingleOrDefault(b => b.ID == sequenceTriggerID);
                            if (result != null)
                            {
                                try
                                {
                                    result.StatusID = 3;
                                    db.SaveChanges();
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.InnerException);
                                    return ex.InnerException.ToString();
                                }
                            }
                        }

                    }
                    else
                    {
                        string errorBody = response.Content;
                        Exception exception = new Exception(errorBody);
                        return exception.ToString();
                    }

                }
                catch (Exception ex)
                {

                    return ex.InnerException.ToString();
                }
            }
            catch (Exception ex )
            {

                return ex.InnerException.ToString();
            }
            return "OK";
        }
    }
}
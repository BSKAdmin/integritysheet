﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegritySheet.Models
{
    public class QuestionData
    {
            public static List<QuestionDataModel> Question = new List<QuestionDataModel> {
            new QuestionDataModel {
                ID = 1,
                Name = "Yes"
            },
            new QuestionDataModel {
                ID = 2,
                Name = "No"
            },
            new QuestionDataModel {
                ID = 3,
                Name = "N/A"
            }
        };
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace IntegritySheet.Models
{
    public class WorkOrderSummary
    {
        public WorkOrderSummary()
        {
            this.spSamples = new List<sp_GetSamplesAndContiainers_Result>();
            this.spCOCResponses = new List<sp_GetCOCResponses_Result>();
            this.containers = new List<SampleLocationContainer>();
            this.splitData = new List<sp_GetSplitData_Result>();
        }

        public int? WorkOrderSummaryID { get; set; }
       
        public sp_GetProject_Result spProject {get; set; }
        public sp_GetReceipt_Result spReceipt { get; set; }
        public List<sp_GetSamplesAndContiainers_Result> spSamples { get; set; }
        public List<sp_GetCOCResponses_Result> spCOCResponses { get; set; }

        public List<SampleLocationContainer> containers { get; set; }
       
        public List<sp_GetSplitData_Result> splitData { get; set; }

    }

   
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegritySheet.Models
{
    public class ResponseData
    {
        public static List<ResponseModel> Responses= new List<ResponseModel> {
            new ResponseModel {
                ID = "P",
                Name = "Pass"
            },
            new ResponseModel {
                ID = "F",
                Name = "Fail"
            },
            
        };
    }
}
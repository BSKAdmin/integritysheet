//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class REPORTQUEUE
    {
        public string QID { get; set; }
        public string Wrk { get; set; }
        public Nullable<System.DateTime> QDate { get; set; }
        public string QUser { get; set; }
        public string QMachine { get; set; }
        public string RptOptions { get; set; }
        public Nullable<System.DateTime> OutputDate { get; set; }
        public string OutputMessage { get; set; }
        public bool OutputError { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string ReportFormat { get; set; }
        public string EDDFormat { get; set; }
        public string AttachPath { get; set; }
        public string InvoiceFormat { get; set; }
        public bool SendCD { get; set; }
        public bool SendRPT { get; set; }
        public Nullable<System.DateTime> MailedCD { get; set; }
        public Nullable<System.DateTime> MailedRPT { get; set; }
    }
}

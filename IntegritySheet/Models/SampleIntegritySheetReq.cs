﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace IntegritySheet.Models
{
    public class SampleIntegritySheetReq
    {
        public int ID { get; set; }
        [StringLength(100, MinimumLength = 0, ErrorMessage = "Maximum 100 characters")]
        public string WorkOrder { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 0, ErrorMessage = "Maximum 100 characters")]
        public string CreatedBy { get; set; }
        [Required]
        public string Customer { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 0, ErrorMessage = "Maximum 100 characters")]
        public string Project { get; set; }
        [StringLength(50, MinimumLength = 0, ErrorMessage = "Maximum 50 characters")]
        public string CopyWorkOrder { get; set; }
        public Nullable<int> StatusID { get; set; }
        [Required]
        [RegularExpression("^\\d+$", ErrorMessage = "Must be a Positive Non-decimal Number")]
        [Range(1, 2147483647)]
        public Nullable<int> TurnAroundTime { get; set; }
        [Required]
        public Nullable<int> ReceivingFacility { get; set; }
        [Required]
        public Nullable<System.DateTime> ReceiveDate { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 0, ErrorMessage = "Maximum 50 characters")]
        public string SubmittedBy { get; set; }
    }
}
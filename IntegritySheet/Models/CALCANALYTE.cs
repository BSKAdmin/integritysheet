//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CALCANALYTE
    {
        public string Analysis { get; set; }
        public string Matrix { get; set; }
        public string Analyte { get; set; }
        public string SubAnalyte { get; set; }
        public string Function { get; set; }
        public string Operand { get; set; }
        public string Units { get; set; }
        public Nullable<float> Factor { get; set; }
        public Nullable<short> AnalyteOrder { get; set; }
        public string SpecificMethod { get; set; }
        public string GeneralMethod { get; set; }
        public bool ExcludeNonRp { get; set; }
        public bool NoMDLMRL { get; set; }
        public Nullable<short> MRLOption { get; set; }
        public bool MergeRE { get; set; }
        public bool RpToMDL { get; set; }
        public bool DryWtResult { get; set; }
        public bool DryWtMRL { get; set; }
        public bool BFlg { get; set; }
        public bool DFlg { get; set; }
        public bool JFlg { get; set; }
        public bool UFlg { get; set; }
        public string EqText { get; set; }
        public Nullable<float> FlagLow { get; set; }
        public Nullable<float> FlagLevel1 { get; set; }
        public Nullable<float> FlagLevel2 { get; set; }
        public Nullable<float> FlagLevel3 { get; set; }
        public Nullable<float> FlagLevel4 { get; set; }
        public Nullable<float> FlagLevel5 { get; set; }
        public string FactorEq { get; set; }
    }
}

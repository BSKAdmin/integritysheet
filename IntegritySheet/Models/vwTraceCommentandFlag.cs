//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwTraceCommentandFlag
    {
        public string Client { get; set; }
        public string Project { get; set; }
        public string Analysis { get; set; }
        public string GroupAnalysis { get; set; }
        public string Comments { get; set; }
        public bool JFlg { get; set; }
    }
}

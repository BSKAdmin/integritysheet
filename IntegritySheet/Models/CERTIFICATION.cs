//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CERTIFICATION
    {
        public string CertID { get; set; }
        public string CertNumber { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Expires { get; set; }
        public string FacilityCode { get; set; }
    }
}

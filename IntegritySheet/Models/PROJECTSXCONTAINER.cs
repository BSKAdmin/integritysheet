//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PROJECTSXCONTAINER
    {
        public string Client { get; set; }
        public string Project { get; set; }
        public string Sample { get; set; }
        public string Container { get; set; }
        public Nullable<short> Quantity { get; set; }
    }
}

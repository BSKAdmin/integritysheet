//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class REPSAMPLEANALYSI
    {
        public string Wrk { get; set; }
        public string Sample { get; set; }
        public string Analysis { get; set; }
        public string Laboratory { get; set; }
        public string GroupAnalysis { get; set; }
        public string SubLab { get; set; }
        public string QCCode { get; set; }
        public string RECode { get; set; }
        public string Matrix { get; set; }
        public string GeneralMethod { get; set; }
        public string SpecificMethod { get; set; }
        public string Department { get; set; }
        public string AnaClass { get; set; }
        public Nullable<float> Hold { get; set; }
        public Nullable<float> HoldA { get; set; }
        public Nullable<short> HoldType { get; set; }
        public Nullable<short> HoldAType { get; set; }
        public Nullable<short> LeachHold { get; set; }
        public Nullable<float> SxToLeach { get; set; }
        public Nullable<float> LeachToPrep { get; set; }
        public Nullable<float> SxToPrep { get; set; }
        public Nullable<float> SxToAnalyzed { get; set; }
        public Nullable<System.DateTime> Exp1 { get; set; }
        public Nullable<System.DateTime> Exp2 { get; set; }
        public string LogComments { get; set; }
        public string ExtractionComments { get; set; }
        public string BatchInfo1 { get; set; }
        public string BatchInfo2 { get; set; }
        public string BatchInfo3 { get; set; }
        public string BatchInfo4 { get; set; }
        public string BatchInfo5 { get; set; }
        public string BatchInfo6 { get; set; }
        public string BatchInfo7 { get; set; }
        public string BatchInfo8 { get; set; }
        public string BatchInfo9 { get; set; }
        public string BatchInfo10 { get; set; }
        public string ExInfo1 { get; set; }
        public string ExInfo2 { get; set; }
        public string ExInfo3 { get; set; }
        public string ExInfo4 { get; set; }
        public string ExInfo5 { get; set; }
        public string Batch { get; set; }
        public string Extraction { get; set; }
        public string ExtractionDescription { get; set; }
        public Nullable<System.DateTime> Extracted { get; set; }
        public string ExtractedBy { get; set; }
        public string LeachBatch { get; set; }
        public Nullable<System.DateTime> Leached { get; set; }
        public string LeachExtraction { get; set; }
        public string LeachedBy { get; set; }
        public string Designator { get; set; }
        public string DesignatorComments { get; set; }
        public string PrepInitialUnits { get; set; }
        public Nullable<float> Initial { get; set; }
        public string sfInitial { get; set; }
        public string PrepFinalUnits { get; set; }
        public Nullable<float> Final { get; set; }
        public string sfFinal { get; set; }
        public string Surrogate { get; set; }
        public Nullable<float> SurrogateAmount { get; set; }
        public Nullable<int> SurrType { get; set; }
        public string SurrogateA { get; set; }
        public Nullable<float> SurrogateAAmount { get; set; }
        public Nullable<int> SurrAType { get; set; }
        public string ISTDID { get; set; }
        public Nullable<float> CalcSolids { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> StatusDate { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> LPrice { get; set; }
        public Nullable<short> Surcharge { get; set; }
        public Nullable<short> LSurcharge { get; set; }
        public Nullable<short> TAT { get; set; }
        public Nullable<short> RTAT { get; set; }
        public bool isBatchQC { get; set; }
        public bool isSequenceQC { get; set; }
        public bool isZZZZZ { get; set; }
        public bool isCustom { get; set; }
        public string SampleNotes { get; set; }
        public string SampleMemo { get; set; }
        public string AnaBatch { get; set; }
        public Nullable<int> AnaOrder { get; set; }
        public string AnaDesignator { get; set; }
        public string AnaDesignatorComments { get; set; }
        public string SubAnaBatch { get; set; }
        public string Calibration { get; set; }
        public string CalRefNum { get; set; }
        public string CalRefFileID { get; set; }
        public Nullable<System.DateTime> AnalyzedMin { get; set; }
        public Nullable<System.DateTime> AnalyzedMax { get; set; }
        public Nullable<System.DateTime> ConfAnalyzedMin { get; set; }
        public Nullable<System.DateTime> ConfAnalyzedMax { get; set; }
        public Nullable<System.DateTime> CalAnalyzedMin { get; set; }
        public Nullable<System.DateTime> CalAnalyzedMax { get; set; }
        public string GPCBatch { get; set; }
        public string GPCStd { get; set; }
        public string FLOBatch { get; set; }
        public string FLOStd { get; set; }
        public string SULBatch { get; set; }
        public string ACDBatch { get; set; }
        public bool isDecanted { get; set; }
        public Nullable<float> CleanupFactor { get; set; }
        public Nullable<float> ExtractionpH { get; set; }
        public Nullable<float> ExtractionSolids { get; set; }
        public Nullable<float> MinDiln { get; set; }
        public Nullable<float> MaxDiln { get; set; }
        public string SeqInst { get; set; }
        public string InstCol1Desc { get; set; }
        public string InstCol1Diam { get; set; }
        public string InstCol1Len { get; set; }
        public string InstCol2Desc { get; set; }
        public string InstCol2Diam { get; set; }
        public string InstCol2Len { get; set; }
        public string MainFileID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool isDissolved { get; set; }
        public bool isTracking { get; set; }
        public bool isLeachMethod { get; set; }
        public Nullable<int> RpOption { get; set; }
        public Nullable<System.DateTime> RptDate { get; set; }
        public bool isFieldAnalysis { get; set; }
        public bool ResultSolids { get; set; }
        public bool MRLSolids { get; set; }
        public string LegacyAnalysis { get; set; }
        public string LegacyMatrix { get; set; }
        public string MigrationBatchID { get; set; }
        public string AnalyzingLaboratory { get; set; }
    }
}

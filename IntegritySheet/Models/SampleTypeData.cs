﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegritySheet.Models
{
    public class SampleTypeData
    {
        public static List<SampleTypeDataModel> SampleSoils = new List<SampleTypeDataModel> {
            new SampleTypeDataModel {
                ID = "W",
                Name = "Water"
            },
            new SampleTypeDataModel {
                ID = "S",
                Name = "Soil"
            },
        };
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace IntegritySheet.Models
{
    public class SplitOrPreserveValidation
    {
        public int ID { get; set; }
        public Nullable<int> SLCID { get; set; }
        //  [Required]
        public string SplitorPreserve1 { get; set; }
          [Required]
           [StringLength(100, MinimumLength = 0, ErrorMessage = "Maximum 100 characters")]

        public string Preservative { get; set; }
          [Required]
        public Nullable<System.DateTime> SplitorPreserveTime { get; set; }
           [Required]
        public Nullable<int> BottleID { get; set; }
           [Required]
        public Nullable<int> SampleAmount { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegritySheet.Models
{
    public class PackingData
    {
            public static List<PackingDataModel> Question = new List<PackingDataModel> {
            new PackingDataModel {
                ID = 1,
                Name = "Foam"
            },
            new PackingDataModel {
                ID = 2,
                Name = "Packing Peanuts"
            },
            new PackingDataModel     {
                ID = 3,
                Name = "Paper"
            },
            new PackingDataModel     {
                ID = 4,
                Name = "Bubble Wrap"
            },
            new PackingDataModel     {
                ID = 5,
                Name = "Other"
            }

        };
        
    }
}

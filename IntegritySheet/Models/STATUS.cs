//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class STATUS
    {
        public string Status1 { get; set; }
        public bool Wrk { get; set; }
        public bool Analysis { get; set; }
        public bool Bid { get; set; }
        public bool Schedule { get; set; }
        public bool Batchable { get; set; }
        public bool Reportable { get; set; }
        public bool Reviewable { get; set; }
        public bool WrkStatic { get; set; }
        public bool RptQuery { get; set; }
        public bool isCancel { get; set; }
        public Nullable<short> OrdinalValue { get; set; }
    }
}
